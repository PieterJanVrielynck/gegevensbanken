# Benodigde packages
import json            # Package om .json files in te laden (bvb kolomnamen zijn zo opgeslagen)
import getpass         # Package om een paswoordveldje te genereren.
import mysql.connector # MySQL package
import numpy as np
import pandas as pd    # Populaire package voor data-verwerking
import sys
import os
import time
import matplotlib.pyplot as plt


def verbind_met_GB(username, hostname, gegevensbanknaam):
    """
    Maak verbinding met een externe gegevensbank
    
    :param  username:          username van de gebruiker, string
    :param  hostname:          naam van de host, string.
                               Dit is in het geval van een lokale server gewoon 'localhost'
    :param  gegevensbanknaam:  naam van de gegevensbank, string.
    :return connection:        connection object, dit is wat teruggeven wordt 
                               door connect() methods van packages die voldoen aan de DB-API
    """
    
    password = getpass.getpass() # Genereer vakje voor wachtwoord in te geven
    
    connection = mysql.connector.connect(host=hostname,
                                         user=username,
                                         passwd=password,
                                         db=gegevensbanknaam)
    return connection


def check_perfect_match(df1, df2):
    """
    Functie om te checken of 2 DataFrames gelijk zijn.
    """
    check = df1.equals(df2)
    return check


def run_query(connection, query):
    """
    Voer een query uit op een reeds gemaakte connectie, geeft het resultaat van de query terug
    """
    
    # Making a cursor and executing the query
    cursor = connection.cursor()
    cursor.execute(query)
    
    # Collecting the result and casting it in a pd.DataFrame
    res = cursor.fetchall()
    
    return res
def run_query_without_res(connection, query ) :
    # Making a cursor and executing the query
    cursor = connection.cursor()
    cursor.execute(query)


def res_to_df(query_result, column_names):
    """
    Giet het resultaat van een uitgevoerde query in een 'pandas dataframe'
    met vooraf gespecifieerde kolomnamen.
    
    Let op: Het resultaat van de query moet dus exact evenveel kolommen bevatten
    als kolomnamen die je meegeeft. Als dit niet het geval is, is dit een indicatie
    dat je oplossing fout is. (Gezien wij de kolomnamen van de oplossing al cadeau doen)
    
    """
    df = pd.DataFrame(query_result, columns=column_names)
    return df



# Dit is de originele query die geoptimaliseerd dient te worden. Niet wijzigen!
def query_to_optimize(connection, column_names, jaar_1=2000, jaar_2=1990, lengte=75):
    # Actual query
    query="""
    SELECT m.birthState, AVG(m.weight), AVG(m.height), AVG(bat.HR), AVG(pit.SV)
    FROM Master AS m,
        pitching AS pit,
        batting AS bat,
        hallOffame AS hof
    WHERE pit.yearID = {}
        AND bat.yearID = {}
        AND pit.playerID = m.playerID
        AND bat.playerID = m.playerID
        AND m.playerID = hof.playerID
        AND hof.yearID > {}
    GROUP BY m.birthState
    HAVING AVG(m.height) > {}
    ORDER BY m.birthState ASC;
    """.format(jaar_1, jaar_1, jaar_2, lengte)
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df = res_to_df(res, column_names)          # Query in DataFrame brengen
    
    return df
	
	
def explain(connection, column_names, jaar_1=2000, jaar_2=1990, lengte=75) :
    query=""" EXPLAIN (
	        SELECT m.birthState, AVG(m.weight), AVG(m.height), AVG(bat.HR), AVG(pit.SV)
            FROM Master AS m,
                 pitching AS pit,
                 batting AS bat,
                 hallOffame AS hof
            WHERE pit.yearID = {}
                  AND bat.yearID = {}
                  AND pit.playerID = m.playerID
                  AND bat.playerID = m.playerID
                  AND m.playerID = hof.playerID
                  AND hof.yearID > {}
            GROUP BY m.birthState
            HAVING AVG(m.height) > {}
            ORDER BY m.birthState ASC ) ; 
	 
	""".format(jaar_1, jaar_1, jaar_2, lengte)

    res = run_query(connection, query)         # Query uitvoeren
    df = res_to_df(res, column_names)          # Query in DataFrame brengen
    
    return df	
	

def add_index(	connection ) :
    query1="""
	AlTER TABLE master ADD CONSTRAINT pk_master PRIMARY KEY (playerID)  ;
    """
    query2 = """
        ALTER TABLE batting ADD CONSTRAINT pk_batting PRIMARY KEY (playerID, stint, yearID, teamID);
        """

    query3 = """
        ALTER TABLE pitching ADD CONSTRAINT pk_pitching PRIMARY KEY (playerID, stint,  yearID, teamID);
        """
    
    query4="""
	ALTER TABLE pitching ADD CONSTRAINT ref_1 FOREIGN KEY (playerID) REFERENCES master (playerID) ;  
	"""
    query5="""
	ALTER TABLE batting ADD CONSTRAINT ref_2 FOREIGN KEY (playerID) REFERENCES master (playerID) ; 
	"""  
    query6="""
	ALTER TABLE hallOffame ADD CONSTRAINT pk_halloffame PRIMARY KEY (playerID, yearID, votedBy);
	"""
    query7="""
	CREATE INDEX ind1  ON hallOffame (yearID) ; 
	"""
    
    run_query_without_res(connection, query1) 
    run_query_without_res(connection, query2) 
    run_query_without_res(connection, query3) 	
    run_query_without_res(connection, query4)
    run_query_without_res(connection, query5)
    run_query_without_res(connection, query6)
    run_query_without_res(connection, query7)
	

def drop_index(	connection ):
    query_drop_fk2="""
	ALTER TABLE batting DROP FOREIGN KEY ref_2 ;  
	"""

    query_drop_fk1="""
	AlTER TABLE pitching DROP FOREIGN KEY ref_1 ; 
	"""

    query_drop_pk="""
	ALTER TABLE master DROP PRIMARY KEY ; 
	"""	
    query_drop_ind1="""
	ALTER TABLE hallOffame DROP INDEX ind1 ; 
	"""

    query_drop_pk2 = """
        ALTER TABLE pitching DROP PRIMARY KEY ;
        """


    query_drop_pk3="""
        ALTER TABLE batting DROP PRIMARY KEY;
        """
    query_drop_pk4="""
	ALTER TABLE hallOffame DROP PRIMARY KEY;
	"""


    
    run_query_without_res(connection,query_drop_fk2)
    run_query_without_res(connection,query_drop_fk1) 
    run_query_without_res(connection, query_drop_pk)
    run_query_without_res(connection, query_drop_ind1)
    run_query_without_res(connection, query_drop_pk2)
    run_query_without_res(connection, query_drop_pk3)
# Maak hier je eerste optimalisatie
def optim_1(connection, column_names, jaar_1=2000, jaar_2=1990, lengte=75):
    query="""
    SELECT m.birthState, AVG(m.weight), AVG(m.height), AVG(bat.HR), AVG(pit.SV)
    FROM Master AS m,
        pitching AS pit,
        batting AS bat,
        hallOffame AS hof
    WHERE pit.yearID = {}
        AND bat.yearID = {}
        AND pit.playerID = m.playerID
        AND bat.playerID = m.playerID
        AND m.playerID = hof.playerID
        AND hof.yearID > {}
    GROUP BY m.birthState
    HAVING AVG(m.height) > {}
    ORDER BY m.birthState ASC;
    """.format(jaar_1, jaar_1, jaar_2, lengte)
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df = res_to_df(res, column_names)          # Query in DataFrame brengen
    
    return df	


def visualizeTHIS(connection, visualize_cols):
    query="""
    SELECT m.birthState, AVG(b.yearID - m.birthYear)
    FROM (master m JOIN (SELECT playerID, yearID, max(HR) FROM Batting GROUP BY playerID) b ON m.playerID = b.playerID)
     JOIN halloffame H ON M.playerID = H.playerID
    WHERE H.inducted = "Y"
    GROUP BY m.birthState;
    """
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df =  res_to_df(res, visualize_cols)         # Query in DataFrame brengen
    return df	
	
username = 'root'      # Vervang dit als je via een andere user queries stuurt
hostname = 'localhost' # Als je een databank lokaal draait, is dit localhost.
db = 'lahman2016'      # Naam van de gegevensbank op je XAMPP Mysql server

# We verbinden met de gegevensbank
c = verbind_met_GB(username, hostname, db)
column_names = ['state', 'avg_weight', 'avg_height', 'avg_homeruns', 'avg_saves']
col_nam = ['id', 'select_type', 'table', 'type' ,  'possible_keys', 'key', 'key_len', 'ref', 'rows', 'extra']

visualize_cols = ['State', 'TopPerformance']


plt.figure(1)
df = visualizeTHIS(c, visualize_cols)
df['A'] = pd.Series(list(range(len(df))))
df=df.astype({'TopPerformance':float}) #FOR SOME REASON IT NEEDS THIS !!!!!!!!!!!!!!!!!!
df=df.astype({'State':str})
ax = df['TopPerformance'].plot.bar(color=[np.random.rand(3,1) for i in range(len(df))])
ax.set_xlabel('Birth State')
ax.set_ylabel('Age of best performance')
ax.set_xticklabels(df.State)

#Visualisation nr 2
def visualizeTHIS2(connection, visualize_cols):
    query="""
    SELECT m.birthYear, AVG(b.yearID - m.birthYear)
    FROM (master m JOIN (SELECT playerID, yearID, max(HR) FROM Batting GROUP BY playerID) b ON m.playerID = b.playerID)
     JOIN halloffame H ON M.playerID = H.playerID
    WHERE H.inducted = "Y" AND m.birthYear < 1995 AND m.birthYEAR > 1800
    GROUP BY m.birthYear;
    """
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df =  res_to_df(res, visualize_cols)         # Query in DataFrame brengen
    return df	

plt.figure(2)
cols2 = ['Year', 'Age']
df = visualizeTHIS2(c, cols2)
df=df.astype({'Age':float})
df=df.astype({'Year':float})
df.plot(x = 'Year', y = 'Age')

def visualizeTHIS3(connection, visualize_cols):
    query="""
    SELECT m.birthYear, AVG(b.yearID - m.birthYear)
    FROM (master m JOIN (SELECT playerID, yearID, max(HR) FROM Batting GROUP BY playerID) b ON m.playerID = b.playerID)
     JOIN halloffame H ON M.playerID = H.playerID
    WHERE m.birthYear < 1995 AND m.birthYEAR > 1800
    GROUP BY m.birthYear;
    """
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df =  res_to_df(res, visualize_cols)         # Query in DataFrame brengen
    return df	

plt.figure(2)
cols2 = ['Year', 'Age']
df = visualizeTHIS3(c, cols2)
df=df.astype({'Age':float})
df=df.astype({'Year':float})
df.plot(x = 'Year', y = 'Age')


def visualizeTHIS4(connection, visualize_cols):
    query="""
    SELECT m.birthYear, AVG(b.yearID - m.birthYear)
    FROM (master m JOIN (SELECT playerID, yearID, max(HR) FROM Batting GROUP BY playerID) b ON m.playerID = b.playerID)
    WHERE m.birthYear < 1995 AND m.birthYEAR > 1800
    GROUP BY m.birthYear;
    """
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df =  res_to_df(res, visualize_cols)         # Query in DataFrame brengen
    return df	

plt.figure(2)
cols2 = ['Year', 'Age']
df = visualizeTHIS4(c, cols2)
df=df.astype({'Age':float})
df=df.astype({'Year':float})
df.plot(x = 'Year', y = 'Age')



def visualizeTHIS5(connection, visualize_cols):
    query="""
    SELECT m.birthYear, AVG((year(finalGame) + year(debut))/2 - m.birthYear)
    FROM master m
    WHERE m.birthYear < 1990 AND m.birthYEAR > 1800
    GROUP BY m.birthYear
    """
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df =  res_to_df(res, visualize_cols)         # Query in DataFrame brengen
    return df	


plt.figure(2)
cols2 = ['Year', 'Age']
df = visualizeTHIS5(c, cols2)
df=df.astype({'Age':float})
df=df.astype({'Year':float})
df.plot(x = 'Year', y = 'Age')

def visualizeTHIS6(connection, visualize_cols):
    query="""
    SELECT m.birthYear, AVG(b.yearID - m.birthYear), AVG((year(finalGame) + year(debut))/2 - m.birthYear)
    FROM (master m JOIN (SELECT playerID, yearID, max(HR) FROM Batting GROUP BY playerID) b ON m.playerID = b.playerID)
    WHERE m.birthYear < 1995 AND m.birthYEAR > 1800
    GROUP BY m.birthYear;
    """
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df =  res_to_df(res, visualize_cols)         # Query in DataFrame brengen
    return df	

plt.figure(2)
cols2 = ['Year', 'MostHomeruns', 'Mean']
df = visualizeTHIS6(c, cols2)
df=df.astype({'MostHomeruns':float})
df=df.astype({'Year':float})
df=df.astype({'Mean':float})
df = df.set_index('Year')
df.plot()

def visualizeTHIS7(connection, visualize_cols):
    query="""
    SELECT m.birthYear, AVG(b.yearID - m.birthYear), AVG((year(finalGame) + year(debut))/2 - m.birthYear)
    FROM (master m JOIN (SELECT playerID FROM Batting GROUP BY playerID) b ON m.playerID = b.playerID)
    WHERE m.birthYear < 1995 AND m.birthYEAR > 1800
    GROUP BY m.birthYear;
    """
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df =  res_to_df(res, visualize_cols)         # Query in DataFrame brengen
    return df	

plt.figure(2)
cols2 = ['Year', 'MostHomeruns', 'Mean']
df = visualizeTHIS7(c, cols2)
df=df.astype({'MostHomeruns':float})
df=df.astype({'Year':float})
df=df.astype({'Mean':float})
df = df.set_index('Year')
df.plot()









def visualizeTHIS8(connection, visualize_cols):
    query="""
    SELECT (b.yearID - m.birthYear), ((year(finalGame) + year(debut))/2 - m.birthYear)
    FROM (master m JOIN (SELECT playerID, yearID FROM Batting GROUP BY playerID) b ON m.playerID = b.playerID)
    WHERE m.birthYear < 1995 AND m.birthYEAR > 1800;
    """
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df =  res_to_df(res, visualize_cols)         # Query in DataFrame brengen
    return df	

plt.figure(2)
cols2 = ['MostHomeruns', 'Mean']
df = visualizeTHIS8(c, cols2)
df=df.astype({'MostHomeruns':float})
df=df.astype({'Mean':float})
df.plot.hist()

plt.show()

def visualizeTHIS9(connection, visualize_cols):
    query="""
    SELECT ((year(finalGame) - year(debut)))
    FROM (master m JOIN (SELECT playerID, yearID FROM Batting GROUP BY playerID) b ON m.playerID = b.playerID)
    WHERE m.birthYear < 1995 AND m.birthYEAR > 1800;
    """
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df =  res_to_df(res, visualize_cols)         # Query in DataFrame brengen
    return df	

plt.figure(2)
print(df['CareerLength'])
cols2 = ['CareerLength']
df = visualizeTHIS9(c, cols2)
lambda1 = 1/df['CareerLength'].mean()
lambda2 = 1/np.sqrt(df['CareerLength'].var())
print(lambda1, lambda2)
df=df.astype({'CareerLength':float})
df.plot.hist()

plt.show()











#ax = df.plot.bar(x='A',y='TopPerformance', color=[plt.cm.summer(np.arange(len(df)))])
#ax.set_xticklabels(df.State)


#timings=[]
#
## We voeren deze query 5 keer uit om een betere schatting te krijgen van de effectieve runtime.
#for i in range(5):
#    t1 = time.time() # Start time
#    df_orig = query_to_optimize(c, column_names, jaar_1=2000, jaar_2=1990, lengte=75)
#    t2 = time.time() # Stop time
#    timings.append(t2-t1) 
#    
#orig_runtime=np.mean(timings) # De uiteindelijke runtime is een gemiddelde van 5 runs.
#print(explain(c, col_nam) ) # explain before indexing 
#print('De originele query duurt: ', "{0:.4f}".format(orig_runtime),' seconden')
#
#
#
#add_index(c) 
## Test hier je eerste optimalisatie
#print(explain(c, col_nam) )
#
#timings_optim_1 = []
#for i in range(5):
#    t1 = time.time() # Start time
#    df_optim_1 = optim_1(c, column_names, jaar_1=2000, jaar_2=1990, lengte=75)
#    t2 = time.time() # Stop time
#    timings_optim_1.append(t2-t1) 
#    
#optim_1_runtime = np.mean(timings_optim_1) # Runtime optimalisatie 1
#
## Vergelijken met originele query
#diff = orig_runtime-optim_1_runtime # Winst t.o.v. origineel
#rel_diff = diff/orig_runtime # Relatieve winst
#check = check_perfect_match(df_orig,df_optim_1) # Nagaan of de resultaten exact gelijk zijn.
#
## Belangrijkste resultaten printen.
#print('De optimalisatie duurt: ', "{0:.2f}".format(optim_1_runtime),' seconden')
#print('De originele versie duurde: ', "{0:.2f}".format(orig_runtime),' seconden')
#print('De netto tijdwinst is dus: ',"{0:.2f}".format(diff),' seconden, oftewel', "{0:.2f}".format(100*rel_diff), '%' )
#print('Is het resultaat equivalent? ', check)
#
#drop_index(c) 
#
#print(explain(c, col_nam) )



#
#
#
#
#timings=[]
#
## We voeren deze query 5 keer uit om een betere schatting te krijgen van de effectieve runtime.
#for i in range(10):
#    t1 = time.time() # Start time
#    df_orig = query_to_optimize(c, column_names, jaar_1=2000, jaar_2=1990, lengte=75)
#    t2 = time.time() # Stop time
#    timings.append(t2-t1) 
#    
#orig_runtime=np.mean(timings) # De uiteindelijke runtime is een gemiddelde van 5 runs.
#print(explain(c, col_nam) ) # explain before indexing 
#print('De originele query duurt: ', "{0:.4f}".format(orig_runtime),' seconden')
#
#
#
#add_index(c) 
## Test hier je eerste optimalisatie
#print(explain(c, col_nam) )
#
#timings_optim_1 = []
#for i in range(10):
#    t1 = time.time() # Start time
#    df_optim_1 = optim_1(c, column_names, jaar_1=2000, jaar_2=1990, lengte=75)
#    t2 = time.time() # Stop time
#    timings_optim_1.append(t2-t1) 
#    
#optim_1_runtime = np.mean(timings_optim_1) # Runtime optimalisatie 1
#
## Vergelijken met originele query
#diff = orig_runtime-optim_1_runtime # Winst t.o.v. origineel
#rel_diff = diff/orig_runtime # Relatieve winst
#check = check_perfect_match(df_orig,df_optim_1) # Nagaan of de resultaten exact gelijk zijn.
#
## Belangrijkste resultaten printen.
#print('De optimalisatie duurt: ', "{0:.2f}".format(optim_1_runtime),' seconden')
#print('De originele versie duurde: ', "{0:.2f}".format(orig_runtime),' seconden')
#print('De netto tijdwinst is dus: ',"{0:.2f}".format(diff),' seconden, oftewel', "{0:.2f}".format(100*rel_diff), '%' )
#print('Is het resultaat equivalent? ', check)
#
drop_index(c) 
#
#print(explain(c, col_nam) )
#


