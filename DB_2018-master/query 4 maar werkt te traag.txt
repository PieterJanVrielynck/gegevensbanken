SELECT DISTINCT T.name, T.Rank, T.W as wins, T.L as losses
FROM master MAS, teams T JOIN (managers MAN JOIN halloffame HOF ON MAN.playerID = HOF.playerID) ON T.teamID = MAN.teamID
WHERE(
    NOT EXISTS(
        SELECT *
        FROM halloffame HOF2
        WHERE HOF2.yearid < 1980 AND HOF2.playerID = HOF.playerID)
    AND NOT EXISTS(
        SELECT *
        FROM managers MAN2
        WHERE MAN2.yearid < 1980 AND MAN.playerID = MAN2.playerID)
    AND MAS.playerID = MAN.playerID)
ORDER BY T.name, T.Rank; 