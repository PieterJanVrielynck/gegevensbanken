# -*- coding: utf-8 -*-
"""
Created on Sun Apr  1 10:36:49 2018

@author: Pieter-Jan Vrielynck
"""

# De benodigde packages        # Package om een paswoordveldje te genereren.
import mysql.connector # MySQL package
import os
import sys
import pandas as pd

# Importeren van onze custom tools
sys.path.append(os.path.join(os.getcwd(), 'src')) # Add src folder to our PATH variable
from execution import *
from evaluation import *

def verbind_met_GB(username, hostname, gegevensbanknaam):
    """
    Maak verbinding met een externe gegevensbank
    
    :param  username:          username van de gebruiker, string
    :param  hostname:          naam van de host, string.
                               Dit is in het geval van een lokale server gewoon 'localhost'
    :param  gegevensbanknaam:  naam van de gegevensbank, string.
    :return connection:        connection object, dit is wat teruggeven wordt 
                               door connect() methods van packages die voldoen aan de DB-API
    """
    
    password = "" # Genereer vakje voor wachtwoord in te geven
    
    connection = mysql.connector.connect(host=hostname,
                                         user=username,
                                         passwd=password,
                                         db=gegevensbanknaam)
    return connection


username = 'root'      # Vervang dit als je via een andere user queries stuurt
hostname = 'localhost' # Als je een gegevensbank lokaal hebt opgezet, is dit localhost.
db = 'lahman2016'      # Naam van de gegevensbank op je XAMPP Mysql server

# We verbinden met de gegevensbank
c = verbind_met_GB(username, hostname, db)

# filename = os.path.join(os.getcwd(), 'scripts', 'example.py') # Dummy example

filename = 'juisteopdracht.py' # Vul filename van je ingevuld script hier in

run_external_script(filename, c) # Run script with provided parameters and column names

print(evaluate_script(filename))