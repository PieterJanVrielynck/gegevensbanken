
def query_01(connection, column_names):
    # Bouw je query
    query="""
    select t.name, t.yearID, t.HR
    from Teams as t
    order by (-t.HR);
    """.format() # TIP: Zo krijg je parameters in de string (samen met `{}` in de string)
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df = res_to_df(res, column_names)          # Query in DataFrame brengen
    
    return df




def query_02(connection, column_names, datum = '1980-01-16'):
    # Bouw je query
    query="""
    SELECT M.nameFirst, M.nameLast, M.birthYear, M.birthMonth, M.birthDay
    FROM master M
    WHERE M.debut > '{}'
	ORDER BY (M.nameLast); 
    """.format(datum) # TIP: Zo krijg je parameters in de string (samen met `{}` in de string)
    
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df = res_to_df(res, column_names)          # Query in DataFrame brengen
    
    return df



def query_03(connection, column_names): #werkt veel te traag
    # Bouw je query
    query="""
	 SELECT DISTINCT  MAS.nameFirst, MAS.nameLast, T.name
    FROM (master MAS JOIN managers MAN ON MAS.playerID = MAN.playerID) JOIN teams T ON (MAN.teamID = T.teamID AND MAN.yearID = T.yearID)
    WHERE MAN.plyrMgr = 'N'
    ORDER BY T.name;

    """
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df = res_to_df(res, column_names)          # Query in DataFrame brengen
    
    return df
	


  
def query_04(connection, column_names,datum_x='1980-01-01', datum_y='1980-01-01'):
    # Bouw je query
    query="""
    SELECT T.name, T.Rank, T.W, T.L, MAS.nameFirst, MAS.nameLast
    FROM (teams T JOIN managers MAN ON (T.teamID = MAN.teamID AND T.yearID = MAN.yearID)) JOIN master MAS ON (MAN.playerID = MAS.playerID)
    WHERE EXISTS(
            SELECT *
            FROM halloffame HOF
            WHERE HOF.playerID = MAN.playerID AND HOF.yearid >= {0})
    AND EXISTS(
            SELECT *
            FROM managers MAN2
            WHERE MAN2.teamID = MAN.teamID AND MAN2.playerID = MAN.playerID AND MAN2.yearID >= {1})
    ORDER BY T.name, T.Rank
    """.format(datum_x[0:4], datum_y[0:4]) # TIP: Zo krijg je parameters in de string (samen met `{}` in de string)
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df = res_to_df(res, column_names)          # Query in DataFrame brengen
    
    return df


def query_05(connection, column_names):
    # Bouw je query
    query="""
    SELECT DISTINCT T.name
    FROM managers M JOIN teams T ON (M.teamID = T.teamID AND M.yearID = T.yearID)
    WHERE M.yearID > 1980 AND M.plyrMgr = 'Y'
    ORDER BY T.name;
    """
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df = res_to_df(res, column_names)          # Query in DataFrame brengen
    
    return df


def query_06(connection, column_names,salaris=20000):
    # Bouw je query
    query="""
    SELECT T.name, T.Rank, T.yearID, T.W, T.L
    FROM teams T
    WHERE (SELECT MIN(S.salary) FROM salaries S WHERE T.yearID = S.yearID AND T.teamID = S.teamID) > {}
    ORDER BY T.W;
    """.format(salaris) # TIP: Zo krijg je parameters in de string (samen met `{}` in de string)
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df = res_to_df(res, column_names)          # Query in DataFrame brengen
    
    return df



	

	
def query_07(connection, column_names):
    # Bouw je query
    query="""
		SELECT M.nameLast , M.nameFirst
		FROM master M
		WHERE NOT EXISTS (
			SELECT *
			FROM (SELECT DISTINCT awardID
			      FROM awardsmanagers )  A 
			WHERE NOT EXISTS (
				SELECT *
				FROM awardsmanagers W 
				WHERE M.playerID = W.playerID AND A.awardID = W.awardID )
			) 
		ORDER BY M.nameLast ; 
    """ # TIP: Zo krijg je parameters in de string (samen met `{}` in de string)
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df = res_to_df(res, column_names)          # Query in DataFrame brengen
    
    return df



def query_08(connection, column_names, jaar=1990, lengte=75):
    # Bouw je query
    # SELECT playerID, sum(HR) FROM batting GROUP BY playerID
    query="""
    SELECT M.birthState, AVG(M.weight), AVG(M.height), AVG(B.`SUM(HR)`), AVG(P.`SUM(SV)`)
    FROM (master M JOIN (SELECT playerID, sum(SV) FROM pitching GROUP BY playerID) P ON (M.playerID = P.playerID)) JOIN (SELECT playerID, sum(HR) FROM batting GROUP BY playerID) B
    ON (B.playerID = M.playerID) JOIN halloffame H ON M.playerID = H.playerID
    WHERE H.yearID > {0} AND H.inducted = "Y"
    GROUP BY M.birthState
    HAVING AVG(M.height) > {1}
    ORDER BY M.birthState ASC;
    """.format(jaar, lengte)
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df = res_to_df(res, column_names)          # Query in DataFrame brengen
    
    return df






def query_09(connection, column_names, jaar=1990):
    # Bouw je query
    # SELECT playerID, sum(HR) FROM batting GROUP BY playerID
    query="""
    SELECT T.yearID, T.name,  T.HR
    FROM teams T
    WHERE T.yearID = {0}
    AND EXISTS (SELECT * FROM
    	(SELECT DISTINCT B.HR FROM teams B WHERE B.yearID = {0} ORDER BY B.HR DESC LIMIT 1,1) HRTABLE
            WHERE HRTABLE.HR = T.HR);
    """.format(jaar)
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df = res_to_df(res, column_names)          # Query in DataFrame brengen
    
    return df





def query_10(connection, column_names, jaar=1990):
    # Bouw je query
    query="""
    SELECT T.yearID, T.name, T.rank, T.G
	FROM teams T 
	WHERE T.yearID = {0} AND
	      EXISTS (SELECT *
		          FROM appearances A 
				  WHERE A.yearID = {0} AND A.teamID = T.teamID AND
				  EXISTS (SELECT AW.playerID, count(*) 
				          FROM awardsplayers AW
						  WHERE AW.playerID = A.playerID AND AW.yearID = {0} 
						  GROUP BY AW.playerID 
						  HAVING count(*) = 1 )
                 )
	ORDER BY T.name, T.rank ; 			        
				        
    """.format(jaar) 
    
    # Stap 2 & 3
    res = run_query(connection, query)         # Query uitvoeren
    df = res_to_df(res, column_names)          # Query in DataFrame brengen
    
    return df